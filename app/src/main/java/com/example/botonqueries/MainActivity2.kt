package com.example.botonqueries

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.PopupWindow

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        val buttonTrousers: Button = findViewById(R.id.buttonTrousers)

        buttonTrousers.setOnClickListener {

            val window = PopupWindow(this)
            val view = layoutInflater.inflate(R.layout.layaout_popup, null)
            window.contentView = view
            val imageView = view.findViewById<ImageView>(R.id.imageView)
            imageView.setOnClickListener {
                window.dismiss()
            }

            window.showAsDropDown(buttonTrousers)
        }

        val buttonCamisetas: Button = findViewById(R.id.buttonCamisetas)

        buttonCamisetas.setOnClickListener {

            val window = PopupWindow(this)
            val view = layoutInflater.inflate(R.layout.popup_camisetas, null)
            window.contentView = view
            val imageView2 = view.findViewById<ImageView>(R.id.imageView2)
            imageView2.setOnClickListener {
                window.dismiss()
            }
            window.showAsDropDown(buttonCamisetas)
        }
    }
}