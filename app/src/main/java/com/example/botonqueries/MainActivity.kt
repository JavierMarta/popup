package com.example.botonqueries

import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


            //POPUP MENU
//            val popup = PopupMenu(this,buttonTrousers)
//            popup.inflate(R.menu.test)
//            popup.setOnMenuItemClickListener{
//                Toast.makeText(this, "item: " + it.title,Toast.LENGTH_SHORT).show()
//                true
//            }
//            popup.show()

            //POPUP WINDOWS

        val buttonTrousers: Button = findViewById(R.id.buttonTrousers)

        buttonTrousers.setOnClickListener {

            val window = PopupWindow(this)
            val view = layoutInflater.inflate(R.layout.layaout_popup, null)
            window.contentView = view
            val imageView = view.findViewById<ImageView>(R.id.imageView)
            imageView.setOnClickListener {
                window.dismiss()
            }

            window.showAsDropDown(buttonTrousers)
        }

        val buttonCamisetas: Button = findViewById(R.id.buttonCamisetas)

        buttonCamisetas.setOnClickListener {

            val window = PopupWindow(this)
            val view = layoutInflater.inflate(R.layout.popup_camisetas, null)
            window.contentView = view
            val imageView2 = view.findViewById<ImageView>(R.id.imageView2)
            imageView2.setOnClickListener {
                window.dismiss()
            }
            window.showAsDropDown(buttonCamisetas)
        }

    }
}